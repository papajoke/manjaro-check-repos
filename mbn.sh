#!/usr/bin/env bash

if [[ -d "mbranch" ]]; then
    # use git repo
    python mbranch/__main__.py "$@" --dev
else
    #in /bin/
    python /usr/share/manjaro-check-repos/__main__.py "$@"
fi
