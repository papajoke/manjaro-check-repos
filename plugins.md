Inspiration docker plugins
 

## configuration ##

create user scripts mbn-action in `~/.config/mbn/mbn-***`

## call ##

call this plugin by `mbn action [-t]`

## list actions ##

run `mbn -h`

---

## write plugin ##

### parameters ###

mbn pass 2 parameters by env

```
PACMAN_BRANCH=testing
PACMAN_ROOT=/var/lib/manjaro-repos
```

### define plugin ###

all plugin return a json if paramater is `get-plugin-metadata`
action name and ShortDescription are display in mbn help

```
{
  "version": "1.0.0",
  "ShortDescription": "Search classic in one branch"
}
```

## example ##

file ~/.config/mbn/mbn-find

```bash
#!/usr/bin/env bash

if [[ "$1" == "get-plugin-metadata" ]]; then
cat << HERE
{
  "version": "1.0.0",
  "ShortDescription": "Search classic in one branch"
}
HERE
exit
fi

shift;  # mbn
shift;  # plugin call

if [[ -z "$PACMAN_ROOT" ]]; then
    echo "not call by manjaro check repos ?"
    exit 2
fi

branch="stable"
if [ -n "$PACMAN_BRANCH" ]; then
    branch="$PACMAN_BRANCH"
fi

# -h or --help ?
if [[ "$*" =~ "-h" ]]; then
    echo -e "My custum help ..."
    echo -e "  classic Search in one branch for example"
    exit 0
fi

cmd="${1,,}"
pacman -Ss "$cmd" --config "${PACMAN_ROOT}/${branch}/etc/pacman.conf" --color "always"

echo "::branch: ${branch}"
env | grep -E "PACMAN"
```
