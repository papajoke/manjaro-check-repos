#!/usr/bin/env bash
cd tests/ &>/dev/null
echo -e "\n:: param: test_file.ClassTest[.test_method]\n"

for filename in test_*.py; do
    printf "\t* ${filename%%.*}."
    awk -F" |\\\(" '/^class/ {print $2}' "$filename"
done
echo ""
python -m unittest "$@" -v
