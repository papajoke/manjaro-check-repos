"""
python -m unittest test_parser.ParserTest.test_action_nolist
python -m unittest -v
"""

import unittest
import config
import sys
from lib.parser import ConsoleParser
from lib.exceptions import BranchError

#https://docs.python.org/fr/3/library/unittest.html

class ParserTest(unittest.TestCase):

    def setUp(self):
        """Initialisation des tests."""
        #self.parser = ConsoleParser([sys.argv[0], 'info', '-a', '-d5', '55', '-u', '36', '-x', '--run'])
        #self.parser.show()
        pass

    def test_parser_action(self):
        parser = ConsoleParser([sys.argv[0], 'info', '-a', '-d5', '55', '-u', '36', '-x', '--run'])
        self.assertEqual(parser.action, 'info')

    def test_action_badplace(self):
        with self.assertRaises(ValueError):
            ConsoleParser([sys.argv[0], '-t', 'info', '-a'])

    def test_parser_action_nolist(self):
        with self.assertRaises(ValueError):
            ConsoleParser([sys.argv[0], 'infoX', '-a'])

    def test_parser_option_bool(self):
        parser = ConsoleParser([sys.argv[0], 'info', '-a', '-d5', '55', '-u', '36', '-x', '--run'])
        self.assertTrue(parser.option('not', 'x'))
        self.assertTrue(parser.option('run'))

    def test_parser_option_value(self):
        parser = ConsoleParser([sys.argv[0], 'info', '-a', '-d5', '55', '-y', '36', '-x', '--run', 'val'])
        self.assertTrue(parser.option('not', 'd') == '5')
        self.assertTrue(parser.option('not', 'y') == '36')
        self.assertTrue(parser.option('run') == 'val')

    def test_parser_option_branch(self):
        parser = ConsoleParser([sys.argv[0], 'info', '55', '-t', '36', '--run', 'val'])
        self.assertTrue(parser.branch == 'testing')
        parser = ConsoleParser([sys.argv[0], 'info', '55', '--testing', '36', '--run', 'val'])
        self.assertTrue(parser.branch == 'testing')
        parser = ConsoleParser([sys.argv[0], 'info', '55', '--testing', '-a', '--run', 'val'])
        self.assertTrue(parser.branch_two == 'archlinux')
        with config.OutputBuffer() as buff:
            parser = ConsoleParser([sys.argv[0], 'info', '55', '-e', '-x', '--run', 'val'])
            self.assertIsNone(parser.branch)
            with self.assertRaises(BranchError):
                ConsoleParser([sys.argv[0], ConsoleParser.WANT[0], 'pacman'])
        with self.assertRaises(BranchError):
            parser = ConsoleParser([sys.argv[0], 'diff', '-a'])
            parser.get_branches()
        parser = ConsoleParser([sys.argv[0], 'diff', '-u', '-a'])
        self.assertTrue(list(parser.get_branches()) == ['archlinux', 'unstable'])

    def test_parser_option_param(self):
        parser = ConsoleParser([sys.argv[0], 'info', 'p1', 'p2', '-g', '--run', 'val', '2'])
        self.assertTrue(parser.values == ['p1', 'p2'])
        parser = ConsoleParser([sys.argv[0], 'info', 'p1', '-f8', 'p2', '--run', 'val', '2'])
        self.assertTrue(parser.values == ['p1'])

    def test_parser_option_concat(self):
        parser = ConsoleParser([sys.argv[0], 'info', 'p1', 'p2', '-gvua', '--run', 'val', '2'])
        self.assertTrue(parser.branch_two == 'unstable')
        self.assertTrue(parser.branch == 'archlinux')
        self.assertTrue(parser.option('verbose', 'v'))
