"""
./tests.sh test_versions
python -m unittest test_versions.VersionsTest.test_version_verbose
python -m unittest -v
"""

import unittest
import config
from mainapp import MainApp
from lib.exceptions import BranchError

class VersionsTest(unittest.TestCase):

    def test_version_ste(self):
        app = MainApp(['', 'versions', 'stefano', '-a', '-u'])
        with config.OutputBuffer() as buff:
            app.versions()
        self.assertTrue(not buff.exception)
        self.assertTrue(' packages' in buff.out)

        app = MainApp(['', 'versions', 'philm@manjaro', '-t', '-u'])
        with config.OutputBuffer() as buff:
            app.versions()
        self.assertTrue(not buff.exception)
        self.assertTrue(' packages' in buff.out)

    def test_version_verbose(self):
        app = MainApp(['', 'versions', 'stefano', '-a', '-u', '-v'])
        with config.OutputBuffer() as buff:
            app.versions()
        self.assertTrue(not buff.exception)
        self.assertTrue(' packages' in buff.out)

    def test_version_branch(self):
        app = MainApp(['', 'versions', 'stefano', '-a', '--archlinux', '-v'])
        with config.OutputBuffer() as buff:
            with self.assertRaises(BranchError):
                app.versions()

    def test_version_pkg_not_exist(self):
        app = MainApp(['', 'versions', 'z4p8acman', '-a', '-u', '--pkg'])
        with config.OutputBuffer() as buff:
            with self.assertRaises(UserWarning):
                app.versions()

    def test_version_pkg(self):
        app = MainApp(['', 'versions', 'pacman', '-a', '-u', '--pkg'])
        with config.OutputBuffer() as buff:
            app.versions()
        self.assertTrue(not buff.err)
        self.assertTrue(' packages' in buff.out)
        self.assertTrue(not buff.exception)
