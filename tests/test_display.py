"""
./tests.sh test_display
python -m unittest test_display.DisplayTest.test_long
python -m unittest -v
"""

import unittest
import config
from lib.displays import DisplayTemplate
from lib.displays import DisplayPkg
from lib.exceptions import TemplateError

from lib.alpm import AlpmBranch

#https://docs.python.org/fr/3/library/unittest.html


class DisplayTest(unittest.TestCase):

    def test_display_long(self):
        temp = DisplayTemplate("{%n : %v:48} {%m:>12}")
        self.assertEqual(temp.templates[0][1], 48)
        self.assertEqual(temp.templates[1][1], 12)
        temp = DisplayTemplate("{%n : %v} {%m:>12}")
        #temp.debug()

    def test_display_push(self):
        temp = DisplayTemplate("{%n %v:13} {%m:>12}")
        temp.push({'n':'Name', 'v':'version', 'm':100})
        self.assertEqual(temp.results[0].find('Name'), 0)
        self.assertEqual(temp.results[0].find('ver'), 5)

    def test_display_long_output(self):
        temp = DisplayTemplate("{%n %v:36} {%m:>6}")
        temp.push({'n':'Name', 'v':'version', 'm':100})
        self.assertEqual(len(temp.results[0]), 36)
        self.assertEqual(len(temp.results[1]), 6)
        self.assertEqual(temp.results[1], '   100')

    def test_display_colors(self):
        temp = DisplayTemplate("{$a%n$n:6}")
        temp.push({'n':'Name', 'v':'version'})
        print(temp.results[0], end='')
        self.assertEqual(len(temp.results[0]), 6 + len(DisplayTemplate.colors['a']) + len(DisplayTemplate.colors['n'])) # NOT 6 with colors codes

        temp = DisplayTemplate("{$d$i%n$n:6}", 'unstable')
        temp.push({'n':'Name', 'v':'version'})
        self.assertEqual(len(temp.results[0]), 6 + len(DisplayTemplate.colors['u']) + len(DisplayTemplate.colors['i']) + len(DisplayTemplate.colors['n'])) # NOT 6 with colors codes
        print(temp.results[0], end='')
        #temp.debug()

class DisplayPkgTest(unittest.TestCase):
    def test_display_alpm(self):
        keys = ['n', 'v', 'p', 'P', 'D', 'u', 'r', 'd', 'G', 'b', 'e', 'm']
        alpm = AlpmBranch()
        pkg = alpm.pkg_get('pacman')
        if pkg:
            temp = DisplayPkg("{$a%n$n %v:32} %n {%d}", 'stable')
            temp.push(pkg)
            self.assertTrue('pacman' in temp.results[0])

            template = '{%' + "}\n{%".join(keys)
            temp = DisplayPkg(template, "testing")
            temp.push(pkg)
            for ret in temp.results:
                self.assertTrue(len(ret) > 1)
            self.assertTrue(int(temp.results[-1]) > 10) # size
