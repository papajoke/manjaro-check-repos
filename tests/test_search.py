"""
./tests.sh test_search
python -m unittest test_search.SearchTest.test_long
python -m unittest -v
"""

import unittest
import config
from mainapp import MainApp
from lib.exceptions import BranchError

class SearchTest(unittest.TestCase):

    def test_search_quiet(self):
        app = MainApp(['', 'search', 'pacman', '-u', '-q'])
        with config.OutputBuffer() as buff:
            app.search()
        self.assertTrue(buff.out.count("\n") < 5)
        self.assertTrue(not buff.exception)
        self.assertTrue(not buff.err)
        self.assertTrue('pacman' in buff.out)
        self.assertTrue('core' in buff.out)

        app = MainApp(['', 'search', 'pacman', '-t'])
        with config.OutputBuffer() as buff:
            app.search()
        self.assertTrue(buff.out.count("\n") > 5)

    def test_search_format(self):
        app = MainApp(['', 'search', 'pacman', '-u', '--format', '%n > %r'])
        with config.OutputBuffer() as buff:
            app.search()
        nb_lines = buff.out.count(">")
        self.assertTrue(nb_lines == buff.out.count("\n") -1)
        self.assertTrue('pacman > core' in buff.out)

    def test_search_desc(self):
        app = MainApp(['', 'search', 'pacman', '-u', '-q', '--desc'])
        with config.OutputBuffer() as buff:
            app.search()
        self.assertTrue(buff.out.count("\n") > 10)
        self.assertTrue('pacui' in buff.out)

    def test_search_field(self):
        user = 'fh@manjaro'
        app = MainApp(['', 'search', user, '-u', '--field', "packager", '--format', '^:%p'])
        with config.OutputBuffer() as buff:
            app.search()
        self.assertTrue(buff.out.count('^') == buff.out.count(user))

    def test_search_branch(self):
        with config.OutputBuffer() as buff:
            with self.assertRaises(BranchError):
                MainApp(['', 'search', 'pacman', '-q', '-z'])
        self.assertTrue('--archlinux' in buff.out)

    def test_search_pkg_not_exist(self):
        app = MainApp(['', 'search', 'zpa8cman', '-u', '-q'])
        with config.OutputBuffer() as buff:
            with self.assertRaises(UserWarning):
                app.search()
