#!/usr/bin/python
"""
 ALPM script

 use one pacman.conf by branch
 pacman -Si linux --config /var/lib/manjaro-repos/archlinux/etc/pacman.conf
 pacman -Si linux50 --config /var/lib/manjaro-repos/testing/etc/pacman.conf
 expac -Ss "%n %v"  --config /var/lib/manjaro-repos/testing/etc/pacman.conf

 #security
 pacman -S linux --config /var/lib/manjaro-repos/archlinux/etc/pacman.conf --logfile /var/lib/manjaro-repos/archlinux --dbonly
 Error HOOK and if not, after: filesystem conflict file

"""

import os
import lib
from lib.parser import ConsoleParser
from lib.actions import SwitchClass
from lib import action


################################################

class MainApp:
    """Main Application parse console arguments"""

    def __init__(self, args):
        """ object initialisation """
        if not os.path.exists(lib.TMP_DIR):
            os.mkdir(lib.TMP_DIR)
        self.params = ConsoleParser(args)
        self.actions = SwitchClass(action.ACTIONS)
        self.action = self.actions.factory(self.params.action, self.params)

    def run(self):
        """ run one action """
        if self.action.test():
            self.action.run()
        else:
            print(self.action.error, self.action.usage().replace(' '*11, ''))
