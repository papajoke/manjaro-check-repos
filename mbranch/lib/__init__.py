
"""
starter module
"""

import re
from pathlib import Path

__all__ = (
    '__version__',
    'version_str',
    'name',
    'PKGNAME',
    'DESCRIPTION',
    'TMP_DIR',
    'read_config_file',
    'read_config_root'
)

__version__ = (0, 9, 9, '')

def name():
    """ Provides a user-friendly name of the project """
    return "mbn"
    #return __name__

def version_str(version=__version__):
    """ Provides the version string for this project """
    result = '.'.join(map(str, version[:3]))
    if len(version) > 3:
        result += '-' + '-'.join(version[3:])
    return result

DESCRIPTION = "mbn - manjaro Multi-Branch Navigator"
TMP_DIR = f"/tmp/{name()}-multi-branches"
PKGNAME = "manjaro-check-repos"

def read_config_file(key: str, default: str = '') -> str:
    """where is installed pacman local databases"""
    result = default
    regex = re.compile(r'^' + key + '( ?)=(?P<path>[^#]+)')
    with open(f"/etc/{PKGNAME}/config.conf", 'r') as conf:
        for line in conf:
            if line.startswith("#"):
                continue
            matchs = regex.match(line)
            if not matchs:
                continue
            path = matchs.group('path')
            if path:
                result = path.strip().rstrip('/')
                break
    return result

def read_config_root() -> str:
    """where is installed pacman local databases"""
    path_root = read_config_file('RootDir', '/var/cache/manjaro-repos')
    path_root = Path(path_root)
    if not path_root.is_absolute():
        path_root = path_root.expanduser()
    try:
        path_root.mkdir(parents=True, exist_ok=True)
    except PermissionError:
        pass
    return str(path_root)
