"""
Application Exceptions
"""

class BranchNotFoundError(FileNotFoundError):
    """ Exception local database not set on disk """
    def __init__(self, message, branch):
        super().__init__(message)
        self.branch = branch
        self.message = message

class BranchError(ValueError):
    """ Exception bad branch """
    def __init__(self, message, branch=""):
        super().__init__(message)
        self.branch = branch
        self.message = message

class TemplateError(ValueError):
    """ Exception bad display template """
    def __init__(self, message, template=""):
        super().__init__(message)
        self.template = template
        self.message = message
