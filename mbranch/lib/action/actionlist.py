"""
        Action list packagers
"""
import os
from lib.actions import Action
from lib.alpm import AlpmBranch


class ActionList(Action):

    @staticmethod
    def usage():
        return "list [--unstable] : packagers listing"

    def run(self):
        """manjaro packagers listing"""
        super().run()
        if not self.params.branch:
            self.params.branch = 'unstable'

        branch = AlpmBranch(self.params.branch)
        self.msg(f"Branch: {branch.color(self.params.branch)}{self.params.branch}{branch.color('none')}")
        user = os.environ.get('USER', '????')
        for packager in sorted(set(pkg.packager for pkg in branch.pkg_search('') if 'manjaro' in pkg.packager)):
            items = packager.split('<')
            try:
                ico = ''
                if user in items[1]:
                    ico = '🤘'
                print(f"{items[0]:28}{items[1].rstrip('>'):32} {ico}")
            except IndexError:
                print(packager)
