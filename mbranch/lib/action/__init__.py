"""
    application actions
"""
from pathlib import Path
import json

from ..actions import ActionPlugin
from .actionusage import ActionUsage
from .actionbranch import ActionBranch
from .actionrm import ActionRm
from .actionlist import ActionList
from .actiontree import ActionTree
from .actionsearch import ActionSearch
from .actioninfo import ActionInfo
from .actiondiff import ActionDiff
from .actioncheck import ActionCheck
from .actionduplicate import ActionDuplicate
from .actiongist import ActionGist
from .actionpactree import ActionPactree
from .actionfiles import ActionFiles
from .actionupdate import ActionUpdate
from .actionprofiles import ActionProfiles
from .actionversions import ActionVersions


ACTIONS = {
    "usage": ActionUsage,
    "branch": ActionBranch,
    "list": ActionList,
    "update" : ActionUpdate,
    "rm": ActionRm,
    "tree": ActionTree,
    "search": ActionSearch,
    "info": ActionInfo,
    "versions": ActionVersions,
    "diff": ActionDiff,
    "check": ActionCheck,
    "duplicates": ActionDuplicate,
    "gist": ActionGist,
    "pactree": ActionPactree,
    "files": ActionFiles,
    "profiles": ActionProfiles,
}

for p in Path('~/.config/mbn/').expanduser().glob('mbn-*'):
    if not p.name[4:]:
        continue
    try:
        ACTIONS[p.name[4:]] = ActionPlugin(p.name[4:])
    except json.decoder.JSONDecodeError:
        print(f" ! plugin error {p.name}")
        continue
