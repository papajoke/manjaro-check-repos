"""
        Action remove
"""
from lib.actions import Action


class ActionBranch(Action):

    @staticmethod
    def usage():
        return """
            branch:
                --archlinux or -a
                --unstable  or -u
                --testing  or -t
                --stable  or -s

            -ts == --testing --stable
            """

    def test(self):
        super().test()
        return False

    def run(self):
        """ nothing """
        super().run()
