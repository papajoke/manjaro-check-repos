"""
        Action diff
"""
from lib.actions import Action
from lib.alpm import AlpmBranch
from lib.displays import DisplayPkg
from lib.exceptions import BranchError


class ActionDiff(Action):

    @staticmethod
    def usage():
        return """
            diff --testing --stable [--rm | --add] : compare two branches, can show only one column
                --rm [optional] : only next remove
                --add [optional] : only add in next branch
                --format '%n %v %e %d %G %b %p %r %u %P %D' only with --rm and --add
            """

    def test(self):
        super().test()
        if not self.params.branch:
            raise BranchError(f"Enter a valid branch {AlpmBranch.BRANCHES}")
        if not self.params.branch_two:
            raise BranchError(f"Enter a second valid branch {AlpmBranch.BRANCHES}")
        return True

    def run(self):
        """Diff package list for 2 branches"""
        super().run()
        branches = self.params.get_branches()
        a_branch = AlpmBranch(branches[0])
        items_a = {item.name for item in a_branch.pkg_search('')}
        b_branch = AlpmBranch(branches[1])
        items_b = {item.name for item in b_branch.pkg_search('')}
        only_in_b = items_b - items_a
        show_add = self.params.option('add')
        show_rm = self.params.option('rm')
        format_model = self.params.option('format')
        if not format_model:
            display = DisplayPkg("%n", 'none')
        nb_packages = 0
        ptitle = f"{branches[0]} ({len(items_a - items_b)})"
        title = f":: {AlpmBranch.color(branches[0])}{ptitle:40}{AlpmBranch.color('none')} / {AlpmBranch.color(branches[1])}{branches[1]} ({len(only_in_b)}){AlpmBranch.color('none')}"

        if show_rm:
            title = f":: removed in {AlpmBranch.color(b_branch.branch)}{b_branch.branch} ({len(only_in_b)}){AlpmBranch.color('none')}"
            print(title, end="\n\n")
            if format_model:
                display = DisplayPkg(format_model, b_branch.branch)
            for item in sorted(only_in_b):  # only not in A
                nb_packages += 1
                print(display.string(b_branch.pkg_get(item)))
            if nb_packages > 40:
                print(f"\n{title}\n")
            return
        if show_add:
            only_in_a = items_a - items_b
            title = f":: news in {AlpmBranch.color(a_branch.branch)}{a_branch.branch} ({len(only_in_a)}){AlpmBranch.color('none')}"
            print(title, end="\n\n")
            if format_model:
                display = DisplayPkg(format_model, a_branch.branch)
            for item in sorted(only_in_a):  # only not in B
                nb_packages += 1
                print(display.string(a_branch.pkg_get(item)))
            if nb_packages > 40:
                print(f"\n{title}")
            return

        # default show 2 colomns
        print(f"\n{title}")
        for item in sorted(items_a ^ items_b):  # all diffs
            nb_packages += 1
            prefix = ''
            if item in only_in_b:
                prefix = f"{prefix:46}"
            print(f"{prefix}{item}")
        if nb_packages > 40:
            print(f"\n{title}\n{nb_packages} packages")
