"""
    Action check deps
"""

import subprocess
from lib.actions import Action
from lib.alpm import AlpmBranch


class ActionCheck(Action):

    @staticmethod
    def usage():
        return "check : Check dependencies"

    def test(self):
        super().test()
        return True

    def run(self):
        """ Check dependencies """
        super().run()
        for branchname in AlpmBranch.BRANCHES:
            if self.params.branch and self.params.branch != branchname:
                continue
            self.msg(f"Check dependencies {AlpmBranch.color(branchname)}{branchname}{AlpmBranch.color('none')}")
            subprocess.call(['pacman', '-Dkk', '--config', f"{AlpmBranch.ROOT}/{branchname}/etc/pacman.conf"])
