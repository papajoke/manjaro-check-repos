"""
        Action remove
"""
import os
import subprocess
from lib.actions import Action
from lib.alpm import AlpmBranch


class ActionRm(Action):

    @staticmethod
    def usage():
        return "rm : remove all local databases"

    def test(self):
        super().test()
        if os.geteuid() != 0:
            raise PermissionError("use sudo for remove local databases")
        return True

    def run(self):
        """remove local databases"""
        super().run()
        if AlpmBranch.ROOT:
            subprocess.check_call(['rm', '-rf', AlpmBranch.ROOT])
            print(f"{AlpmBranch.ROOT} removed")
