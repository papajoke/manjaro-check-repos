"""
    Action display help
"""

import lib
from lib.actions import Action, ActionPlugin
#from lib.alpm import AlpmBranch


class ActionUsage(Action):

    @staticmethod
    def usage():
        return "General Help"

    def run(self):
        """display general usage"""
        super().run()
        #sep = "------"
        print(f":: {lib.DESCRIPTION} {lib.version_str()}")
        print(f"run `{lib.name()} -h` for global help or `{lib.name()} \"action\" -h` for action help")
        txt = """
                versions [packager] --unstable --archlinux [-q] [--verbose | -v]: compare version over branches
                diff --testing --stable [--rm | --add] : compare two branches, can show only one column
                check : Check dependencies
                duplicates : find package duplicates over repos
                profiles : Check manjaro m-a profiles
                ------
                info pkgname --unstable [--quiet or -q] [--files [--filter value]] : pacman -Si one branch
                search regex --unstable [--desc] [--quiet or -q] [--packager] [--dep] : search in one branch, by default only in pkgname
                files regex --unstable : search in files path
                pactree : as original command, only --sync
                ------
                tree : local database tree informations
                gist : last boxit annonces
                list [--unstable] : packagers listing
                ------
                update [--force or -f] [--files] [--mirrors | -m] : update all branches
                rm : remove all local databases"""
        print(txt.replace('               ', ''))

        print()
        for a in lib.action.ACTIONS:
            aa = lib.action.ACTIONS[a]
            if isinstance(aa, lib.actions.ActionPlugin):
                print(f" *{a} :", aa.usage())
