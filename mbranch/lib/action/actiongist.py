"""
        Action gist
"""

import urllib
import json
from lib.actions import Action
from lib.alpm import AlpmBranch


class ActionGist(Action):

    @staticmethod
    def usage():
        return "gist : last boxit annonces"

    def run(self):
        """ List annonces """
        super().run()
        url = "https://api.github.com/users/philmmanjaro/gists"
        try:
            with urllib.request.urlopen(url) as f_url:
                req = f_url.read().decode('utf-8')
        except urllib.error.HTTPError:
            return
        gists = json.loads(req)
        print("")
        filters = ['stable', 'testing', 'unstable']
        for filt in filters:
            print(f"{AlpmBranch.color(filt)}::{AlpmBranch.color('none')} {filt}")
            for gist in gists:
                desc = gist['description']
                if desc.lower().find(' update') < 1:
                    continue
                if desc.lower().find(f"[{filt}") < 0:
                    continue
                print(desc)
                for f_url in gist['files']:
                    url_file = gist['files'][f_url]['raw_url']
                    if url_file:
                        print(f"  {AlpmBranch.color('i')}{url_file}{AlpmBranch.color('none')}")
                    break
                break
        '''
        import xml.dom.minidom
        print("")
        for filter in filters:
            print(f"\n{AlpmBranch.color(filter)}::{AlpmBranch.color('none')} {filter}")
            url = "https://forum.manjaro.org/c/announcements/stable-updates.rss"
            with urllib.request.urlopen(url) as f:
                req = f.read().decode()
            dom = xml.dom.minidom.parseString(req)

            def getText(nodelist):
                rc = []
                for node in nodelist:
                    if node.nodeType == node.TEXT_NODE:
                        rc.append(node.data)
                return ''.join(rc)

            titles = dom.getElementsByTagName("title")
            for title in titles:
                desc = getText(title.childNodes)
                if desc.startswith('['):
                    print(desc)
                    break
            first = True
            titles = dom.getElementsByTagName("link")
            for title in titles:
                url_file = getText(title.childNodes)
                if not first:
                    print(f"  {AlpmBranch.color('i')}{url_file}{AlpmBranch.color('none')}")
                    break
                first = False
        '''
