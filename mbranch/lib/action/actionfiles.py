"""
        Action files
"""

from lib.actions import Action
from lib.alpm import AlpmBranch
from lib.exceptions import BranchError


class ActionFiles(Action):

    @staticmethod
    def usage():
        return "files regex --unstable : search in files path\n" + \
            "\tif absolute path can find in all branches"

    def test(self):
        super().test()
        if not self.params.values:
            raise ValueError(f"Nothing to find !")
        if len(self.params.values[0]) < 3:
            raise ValueError(f"Nothing to find !")
        if not self.params.branch and not self.params.values[0].startswith('/'):
            raise BranchError(f"Enter a valid branch {AlpmBranch.BRANCHES}")
        return True

    def run(self):
        """ search in packages files """
        super().run()
        to_find = self.params.values.pop(0)
        for branch_name in AlpmBranch.BRANCHES:
            if self.params.branch and self.params.branch != branch_name:
                continue
            branch = AlpmBranch(branch_name)
            self.msg(f"Branch: {branch.color(branch_name)}{branch_name}{branch.color('none')}")
            try:
                branch.find_files(to_find, not self.params.branch)
            except FileNotFoundError as error:
                if self.params.branch:
                    raise error
                else:
                    print('\tNo files database ...')
