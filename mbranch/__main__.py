#!/usr/bin/python
"""
    Application
"""

import sys
import subprocess
from lib.exceptions import BranchError
from lib.alpm import AlpmBranch
from lib import TMP_DIR
from mainapp import MainApp
from lib.action.actionbranch import ActionBranch

def error(code, err):
    """ display Exceptions """
    ico = {1: "🤥", 10: "🌿"}
    if code < 10:
        msg = str(err)
    else:
        msg = f"\33[31mError:{AlpmBranch.color('none')} {type(err).__name__}: {str(err)}"
    print("\n", msg, f"{ico.get(code,'🤕')}\n", file=sys.stderr)
    exit(code)

if __name__ == '__main__':
    DEBUG = False
    if DEBUG or '--DEBUG' in sys.argv:
        MainApp(sys.argv).run()
    else:
        try:
            MainApp(sys.argv).run()
        except UserWarning as err:
            error(1, err)
        except BranchError as err:
            print(ActionBranch.usage())
            error(10, err)
        except Exception as err:
            error(99, err)
        finally:
            subprocess.run(['rm', '-rf', TMP_DIR])
    print("")
