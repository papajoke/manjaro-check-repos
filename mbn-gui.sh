#!/usr/bin/env bash

if [[ -d "mbranch" ]]; then
    # use git repo
    python mbranch/mbn-gui.py "$@" --dev
else
    python /usr/share/manjaro-check-repos/mbn-gui.py "$@"
fi
