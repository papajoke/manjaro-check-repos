Multi-Branch-Navigator   
use four branches with same manjaro (archlinux, unstable, testing, stable)

## configuration ##

 * `/etc/manjaro-check-repos/config.conf`
    RootDir : path storage for 4 local databases (can use ~/.cache/manjaro-repos)

 * `/etc/manjaro-check-repos/{manjaro,archlinux}.conf`
    we can change server, branch is rewrite by script
    not edit `RootDir` and `DBPath` : rewrite by script

---

## Actions ###

run `mbn -h` for global help or `mbn "action" -h` for action help

---

branch parameter :
   * --archlinux        or -a
   * --unstable         or -u
   * --testing          or -t
   * --stable           or -s

---

`diff` and `versions` compare branches

#### `diff` ####
Package exists
 * branch
 * branch
 * --rm [optional] : only next remove
 * --add [optional] : only add in next branch
 * --format '%n %v %e %d %G %b %p %r %u %P %D' only with --rm and --add

![diff](https://i.postimg.cc/htQ2S3D1/acapture-20190421-diff.png "diff")


#### `versions` ####
compare versions
 * branch
 * branch
 * user [optional] : packager filter as stefano@manjaro
 * --verbose or -v [optional] : show all differences
 * --quiet or -q [optional] : show only package names
 * --aur : only if archlinux selected
 * --pkg [pkgname] : test package dependencies

![versions](https://i.postimg.cc/yNFnRgSJ/acapture-20190421-versions.png "versions")

### `check` ### 
Check dependencies
 * branch

### `duplicates` ### 
find package duplicates over repos
 * branch

### `profiles` ###
check m-a profiles

---

### `update` ### 
update local databases
 * branch [optional]
 * --force or -f [optional] : -Syy
 * --files [optional] : -Fy
 * --mirrors or -m [optional] : change server

### `rm` ### 
remove local databases
usefull before uninstall or after editing .conf

---

#### `info` #### 
pacman -Si
 * branch [optional], if not show 4 branches
 * --quiet or -q
 * --files
 * --filter value : filter for --files (as | grep value)

![info](https://i.postimg.cc/XJ4Lkjws/acapture-201-info.png "info")

#### `search` ####
pacman -Ss
search package in database by package name
 * regex
 * branch
 * --quiet or -q
 * --dep dependance [optional]
 * --packager [optional] packagerEmail [optional] : if not "packagerEmail" packagerEmail is regex
 * --desc [optional] : search in description and package name
 * --field field_name : search "regex" in one field (name, url, buildate,...)
 * --format '%n %v %e %d %G %b %p %r %u %P %D' (as expac)

#### `files` ####
pacman -Fsx
search in package files
 * regex to find
 * branch

#### `pactree` ####
only --sync

---

### `tree` ###
databases infos

### `gist` ###
boxit annonces

### `list` ###
packagers list

---

#### --format ####
use simple cote and not double

   * Values (as expac)

      '%n %v %e %d %G %b %p %r %u %P %D'

   * Colors
      * $a : archlinux
      * $u : unstable
      * $t : testing
      * $s : stable
      * $d : default branch
      * $i : italic
      * $n : stop color

   * Size

      `{...:12}` : 12 characters align left

      `{...:>12}` : 12 characters align right

Examples :

 `--format '{$d%n$n %v:38} {(%r):>14} {%m:>8} Ko'` color name, version, repo and size

 `--format '{$d%n$n %v:32} (%r)\n\t$i%d$n'` color name, version, repo and other line: italic description

 `--format '{$i%b$n \t %n :52} %p'` date, name and packager

 `diff -s -t --format '{$i%b$n  %n::38} %d' --add` with action diff new packages in stable with description
